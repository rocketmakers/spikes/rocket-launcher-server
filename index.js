#!/usr/bin/env node

// require the module as normal
var bs = require("browser-sync").create();
var QRCode = require('qrcode');
var fs = require('fs-extra');
var path = require('path');
var figlet = require('figlet');
var colors = require('colors');

figlet.text('Rocketmakers', {
    font: 'Speed'
}, (error, data) => {
    console.log(data.red);
    console.log(`[ Rocket Launcher v0.0.1-alpha ]`.white);
    console.log();

    bs.init({
        server: path.resolve(process.cwd(), "platforms/android/assets/www"),
        open: false,
        files: [path.resolve(process.cwd(), "platforms/android/assets/www/*"), path.resolve(process.cwd(), "platforms/ios/www/*")],
        plugins: [require("browser-sync-logger")]
    }, (error, b) => {
        let url = bs.getOption('urls').get('external');
        QRCode.toString(url, (err, qr) => {
            console.log();
            console.log(qr.white.inverse);
            console.log();
            console.log(`Scan the QR code in the companion app or use IP ${url} to get started.`.white);
        });
    });

    bs.emitter.on('client:connected', (a)=> {
        console.log();
        console.log(`[ Client Connected ]`.green);
        console.log(`Client ${JSON.stringify(a.ua)}`.white);
    });

    bs.watch(path.resolve(process.cwd(), "www/*"), (event, file) => {
      console.log(`${event} - ${file}`)
        if (event === "change") {
            console.log(`pushing changes to ${file}`.white)
            fs.copySync(path.resolve(process.cwd(), file),
                path.resolve(process.cwd(), `platforms`, `android`, `assets`, file),
                { overwrite: true });

            fs.copySync(path.resolve(process.cwd(), file),
                path.resolve(process.cwd(), `platforms`, `ios`, file),
                { overwrite: true });
        }
        // if (event === "addDir"){
        //   // copy in new directories
        //   fs.copySync(path.resolve(process.cwd(), file),
        //   path.resolve(process.cwd(), `platforms`, `android`, `assets`, file),
        //   { overwrite: true });
        // }
    });

});



